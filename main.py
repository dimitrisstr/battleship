import os
import pygame
from grid import State, Grid
from agent import Agent

text_color = (255, 176, 28)
player_color = (0, 78, 220)
enemy_color = (230, 0, 0)

# screen variables
min_screen_coord = (0, 0)
max_screen_coord = (768, 920)
screen_res = max_screen_coord

# game parameters
cell_size = 38
main_font_size = 24
info_font_size = 16
FPS = 30

# game resources
font_file = 'GearsOfPeace.ttf'


def write(font, message, color):
    text = font.render(str(message), True, color)
    text = text.convert_alpha()

    return text


def info_string(font, color, grid, indexes):
    location = grid.get_location(indexes)
    state = grid.get_state(indexes)
    if state == State.MISS:
        state_string = 'MISS'
    elif state == State.HIT:
        state_string = 'HIT'
    else:
        state_string = "UNKNOWN"
    text = write(font, location[0] + location[1] + ' ' + state_string, color)
    return text


def main():
    pygame.init()
    pygame.display.set_caption("Battleship")
    screen = pygame.display.set_mode(screen_res)

    grid_background_path = os.path.join('sprites', 'sea_grid.png')
    grid_background = pygame.image.load(grid_background_path).convert()
    background = pygame.Surface(screen.get_size())
    background.fill((0, 0, 0))
    background = background.convert()

    # load fonts
    main_font = pygame.font.Font(os.path.join('fonts', font_file), main_font_size)
    info_font = pygame.font.Font(os.path.join('fonts', font_file), info_font_size)

    grid_x = (screen_res[0] - (Grid.columns + 1) * cell_size) // 2
    margin_y = 50
    grid_height = Grid.rows * cell_size

    while True:
        enemy_grid = Grid((grid_x, margin_y), cell_size, grid_background)
        player_grid = Grid((grid_x, 2 * margin_y + grid_height), cell_size, grid_background)
        ai_agent = Agent()

        clock = pygame.time.Clock()

        screen.blit(background, (0, 0))
        text = write(main_font, 'PLACE YOUR SHIPS', text_color)
        screen.blit(text, ((screen_res[0] - text.get_width()) / 2, screen_res[1] / 4))

        player_grid.place_ships(screen)
        enemy_grid.place_ships(screen, True)

        ai_turn = True
        enemy_hit_cell = None
        player_hit_cell = None
        show_enemy_ships = False
        game_running = True

        while game_running:
            clock.tick(FPS)

            mouse_position = pygame.mouse.get_pos()
            for event in pygame.event.get():
                if event.type == pygame.MOUSEBUTTONUP and not ai_turn:
                    temp = enemy_grid.convert_to_indexes(mouse_position)
                    if temp is not None:
                        if enemy_grid.hit(temp):  # if hit is valid
                            player_hit_cell = temp
                            ai_turn = True
                elif event.type == pygame.QUIT:
                    exit()
                elif event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                    exit()

            if ai_turn:
                enemy_hit_cell = ai_agent.play()
                if player_grid.hit(enemy_hit_cell):
                    ai_turn = False
                    if player_grid.get_state(enemy_hit_cell) == State.HIT:
                        ai_agent.successful_hit()

            # Draw
            enemy_hitpoints = write(info_font, 'ENEMY HITPOINTS: ' +
                                    str(enemy_grid.get_hitpoints()), enemy_color)
            friendly_hitpoints = write(info_font, 'FRIENDLY HITPOINTS: ' +
                                       str(player_grid.get_hitpoints()), player_color)
            screen.blit(background, (0, 0))
            if enemy_hit_cell is not None:
                enemy_action_text = info_string(info_font, enemy_color, player_grid, enemy_hit_cell)
                screen.blit(enemy_action_text, ((grid_x - enemy_action_text.get_width()) / 2, 0.75 * screen_res[1]))
            if player_hit_cell is not None:
                player_action_text = info_string(info_font, player_color, enemy_grid, player_hit_cell)
                screen.blit(player_action_text, ((grid_x - player_action_text.get_width()) / 2, 0.25 * screen_res[1]))

            screen.blit(enemy_hitpoints, (0, 0))
            screen.blit(friendly_hitpoints, (0, 32))
            if player_grid.get_hitpoints() == 0 or enemy_grid.get_hitpoints() == 0:
                message = 'DEFEAT' if player_grid.get_hitpoints() == 0 else 'VICTORY'
                game_running = False
                text = write(main_font, message, text_color)
                screen.blit(text, ((screen_res[0] - text.get_width()) / 2, margin_y // 2))

            if not game_running:
                show_enemy_ships = True
            enemy_grid.show(screen, show_enemy_ships)
            enemy_grid.show_selection(screen, enemy_grid.convert_to_indexes(mouse_position))
            player_grid.show(screen)
            player_grid.show_selection(screen, enemy_hit_cell)
            pygame.display.flip()

        wait = True
        while wait:
            event = pygame.event.wait()
            if event.type == pygame.KEYDOWN and event.key == pygame.K_r:  # replay
                wait = False
            elif event.type == pygame.QUIT:
                exit()
            elif event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                exit()


if __name__ == '__main__':
    main()
