from grid import Grid
from random import choice


class Agent:
    def __init__(self):
        self.available_moves = []
        for i in range(Grid.rows):
            for j in range(Grid.columns):
                self.available_moves.append((i, j))

        self.checkerboard_moves = []
        for move in self.available_moves:
            if (move[0] % 2 != 0 and move[1] % 2 != 1) \
                    or (move[0] % 2 != 1 and move[1] % 2 != 0):
                self.checkerboard_moves.append(move)

        self.max_index = -1
        self.min_index = 100
        self.ship_horizontal = False
        self.ship_identified = False
        self.ship_hits = []  # stores only the current ship hits
        self.hit_history = []  # stores all hits
        self.last_hit = None
        self.last_move = None

    def play(self):
        self._clean()  # remove useless moves
        # find a move
        if self.last_hit is None:  # random move if there isn't a known ship
            move = self._random_move()
        else:
            self._identify_ship_orientation()

            if not self.ship_identified:  # ship orientation isn't yet identified
                available_neighbour_cells = self._get_available_nswe(self.last_hit)
                if available_neighbour_cells:
                    move = choice(available_neighbour_cells)
                else:  # random move
                    move = self._random_move()
            else:
                move = None
                while move is None:
                    value = self.last_hit[0] if self.ship_horizontal else self.last_hit[1]

                    # find min and max column if ship is horizontal
                    # or min and max row if ship is vertical
                    for row, column in self.ship_hits:
                        temp = column if self.ship_horizontal else row
                        if temp > self.max_index:
                            self.max_index = temp
                        if temp < self.min_index:
                            self.min_index = temp

                    if self.ship_horizontal and (value, self.max_index + 1) in self.available_moves:
                        move = (value, self.max_index + 1)
                    elif self.ship_horizontal and (value, self.min_index - 1) in self.available_moves:
                        move = (value, self.min_index - 1)
                    elif not self.ship_horizontal and (self.max_index + 1, value) in self.available_moves:
                        move = (self.max_index + 1, value)
                    elif not self.ship_horizontal and (self.min_index - 1, value) in self.available_moves:
                        move = (self.min_index - 1, value)
                    elif self.ship_horizontal and (value, self.max_index + 1) in self.hit_history:
                        self.last_hit = (value, self.max_index + 1)
                        self.ship_hits.append(self.last_hit)
                    elif self.ship_horizontal and (value, self.min_index - 1) in self.hit_history:
                        self.last_hit = (value, self.min_index - 1)
                        self.ship_hits.append(self.last_hit)
                    elif not self.ship_horizontal and (self.max_index + 1, value) in self.hit_history:
                        self.last_hit = (self.max_index + 1, value)
                        self.ship_hits.append(self.last_hit)
                    elif not self.ship_horizontal and (self.min_index - 1, value) in self.hit_history:
                        self.last_hit = (self.min_index - 1, value)
                        self.ship_hits.append(self.last_hit)
                    else:
                        move = self._random_move()
                        self._reset()

        self.available_moves.remove(move)
        if move in self.checkerboard_moves:
            self.checkerboard_moves.remove(move)
        self.last_move = move
        return move

    def successful_hit(self):
        """
        Update hit variables
        """
        self.last_hit = self.last_move
        self.ship_hits.append(self.last_hit)
        self.hit_history.append(self.last_hit)

    def _random_move(self):
        if self.checkerboard_moves:  # not empty
            move = choice(self.checkerboard_moves)
            self.checkerboard_moves.remove(move)
        else:
            for hit in self.hit_history:
                neighbour_cells = self._get_available_nswe(hit)
                if neighbour_cells:
                    move = neighbour_cells[0]
                    break
        return move

    def _get_nswe(self, cell):
        row, column = cell
        return [(row - 1, column), (row + 1, column), (row, column - 1), (row, column + 1)]

    def _get_available_nswe(self, cell):
        """
        Return available north, southm west, east cells
        :param cell: (row, column
        :return: list of neighbour cells (north, south, west, east)
        """
        nswe_cells = []
        for element in self._get_nswe(cell):
            if element in self.available_moves:
                nswe_cells.append(element)
        return nswe_cells

    def _identify_ship_orientation(self):
        """
        Identifies the ship as horizontal or vertical
        """
        if not self.ship_identified:
            row = self.ship_hits[0][0]

            for i in range(1, len(self.ship_hits)):
                if self.ship_hits[i][0] == row:
                    self.ship_horizontal = True
                else:
                    self.ship_horizontal = False
                self.ship_identified = True

    def _clean(self):
        """
        Remove useless moves
        """
        for move in self.available_moves:
            neighbour_cells = self._get_nswe(move)
            available_neighbour_cells = self._get_available_nswe(move)
            result = any(cell in self.hit_history for cell in neighbour_cells)

            if not available_neighbour_cells and not result:
                self.available_moves.remove(move)
                if move in self.checkerboard_moves:
                    self.checkerboard_moves.remove(move)

    def _reset(self):
        self.max_index = -1
        self.min_index = 100
        self.ship_identified = False
        self.ship_hits.clear()
        self.last_hit = None
