import pygame


class Ship:
    def __init__(self, sprite_path, size, cell_size):
        self.sprite = pygame.image.load(sprite_path).convert_alpha()
        self.sprite = pygame.transform.scale(self.sprite, (size * cell_size, cell_size))

        self.size = size
        self.position = None

    def rotate(self):
        self.sprite = pygame.transform.rotate(self.sprite, 90)

    def get_size(self):
        return self.size

    def set_position(self, position):
        """
        :param position: (x, y)
        """
        self.position = position

    def show(self, screen):
        if self.position is not None:
            screen.blit(self.sprite, self.position)
