import os
import pygame
from ship import Ship
from enum import Enum
from random import randint, choice

LEFT = 1
RIGHT = 3


class State(Enum):
    EMPTY = 0
    FULL = 1
    HIT = 2
    MISS = 3


class Grid:
    rows = 10
    columns = 10
    columns_names = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']
    rows_names = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10']

    def __init__(self, position, cell_size, background):
        self.background = pygame.transform.scale(background, (cell_size * (Grid.columns + 1),
                                                              cell_size * (Grid.rows + 1)))
        self.explosion = pygame.image.load(os.path.join('sprites', 'explosion.png')).convert_alpha()
        self.explosion = pygame.transform.scale(self.explosion, (cell_size, cell_size))

        self.smoke = pygame.image.load(os.path.join('sprites', 'smoke.png')).convert_alpha()
        self.smoke = pygame.transform.scale(self.smoke, (cell_size, cell_size))

        self.red_square = pygame.image.load(os.path.join('sprites', 'red_square.png')).convert_alpha()
        self.red_square = pygame.transform.scale(self.red_square, (cell_size, cell_size))

        self.gray_square = pygame.image.load(os.path.join('sprites', 'gray_square.png')).convert_alpha()
        self.gray_square = pygame.transform.scale(self.gray_square, (cell_size, cell_size))

        self.position = position
        # grid position without column and row names
        self.grid_position = (position[0] + cell_size, position[1] + cell_size)
        self.cell_size = cell_size
        self.hitpoints = 0

        self.ships = []
        self._create_ships()
        for ship in self.ships:
            self.hitpoints += ship.get_size()

        # 10 x 10 grid
        self.grid = [[State.EMPTY for i in range(Grid.columns)] for j in range(Grid.rows)]

    def _create_ships(self):
        medium_ship_path = os.path.join('sprites', 'ship_medium.png')
        large_ship_path = os.path.join('sprites', 'ship_large.png')

        for i in range(3):
            small_ship = Ship(medium_ship_path, 2, self.cell_size)
            self.ships.append(small_ship)

        for i in range(3):
            medium_ship = Ship(medium_ship_path, 3, self.cell_size)
            self.ships.append(medium_ship)

        for i in range(2):
            large_ship = Ship(large_ship_path, 4, self.cell_size)
            self.ships.append(large_ship)

        extra_large_ship = Ship(large_ship_path, 6, self.cell_size)
        self.ships.append(extra_large_ship)

    def place_ships(self, screen, random=False):
        def place(cell_list):
            ship.set_position(self.convert_to_coordinates(cell_list[0]))
            if not horizontal:
                ship.rotate()
            for cell in cell_list:
                self.grid[cell[0]][cell[1]] = State.FULL

        for ship in self.ships:
            horizontal = True if not random else choice([True, False])

            ship_placed = False
            while not ship_placed:
                selected_cells = []
                ship_size = ship.get_size()

                if not random:  # mouse position indexes
                    mouse_position = pygame.mouse.get_pos()
                    indexes = self.convert_to_indexes(mouse_position)
                else:  # random indexes
                    row = randint(0, Grid.rows - 1)
                    column = randint(0, Grid.columns - 1)
                    indexes = (row, column)

                # if indexes are inside grid and cell is empty
                if indexes is not None and self.grid[indexes[0]][indexes[1]] == State.EMPTY:
                    selected_cells.append(indexes)

                    # check if the other cell indexes are valid
                    for i in range(ship_size - 1):
                        if horizontal:
                            indexes = (indexes[0], indexes[1] + 1)
                        else:
                            indexes = (indexes[0] + 1, indexes[1])
                        if indexes[0] < Grid.rows and indexes[1] < Grid.columns and \
                                self.grid[indexes[0]][indexes[1]] == State.EMPTY:
                            selected_cells.append(indexes)
                        else:
                            break

                for event in pygame.event.get():
                    if event.type == pygame.MOUSEBUTTONUP and event.button == LEFT:
                        if len(selected_cells) == ship_size:  # all cell indexes are valid
                            ship_placed = True
                            place(selected_cells)
                    elif event.type == pygame.MOUSEBUTTONUP and event.button == RIGHT:
                        horizontal = not horizontal
                    elif event.type == pygame.QUIT:
                        exit()
                    elif event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                        exit()

                self.show(screen, not random)  # hide ships if AI is placing the ships
                if len(selected_cells) == ship_size:
                    if random:
                        ship_placed = True
                        place(selected_cells)
                    for cell in selected_cells:
                        screen.blit(self.gray_square, self.convert_to_coordinates(cell))
                pygame.display.flip()

    def get_hitpoints(self):
        return self.hitpoints

    def get_state(self, indexes):
        """
        :param indexes: (row, column)
        :return: EMPTY or FULL or HIT or MISS
        """
        return self.grid[indexes[0]][indexes[1]]

    def get_location(self, indexes):
        """
        :param indexes: (row, column
        :return: (column name, row name)
        """
        return Grid.columns_names[indexes[1]], Grid.rows_names[indexes[0]]

    def convert_to_indexes(self, position):
        """
        :param position: (x, y)
        :return: (row, column) or None if it is out of bounds
        """
        indexes = ((position[1] - self.grid_position[1]) // self.cell_size,
                   (position[0] - self.grid_position[0]) // self.cell_size)
        if 0 <= indexes[1] < Grid.columns and 0 <= indexes[0] < Grid.rows:
            return indexes
        else:
            return None

    def convert_to_coordinates(self, indexes):
        """
        :param indexes: (row, column)
        :return: (x, y)
        """
        return (self.grid_position[0] + indexes[1] * self.cell_size,
                self.grid_position[1] + indexes[0] * self.cell_size)

    def hit(self, indexes):
        """
        :param indexes: (row, column)
        :return: True if hit is valid
        """
        row, column = indexes
        cell_value = self.grid[row][column]
        if cell_value != State.HIT and cell_value != State.MISS:
            if cell_value == State.EMPTY:
                self.grid[row][column] = State.MISS
            elif cell_value == State.FULL:
                self.grid[row][column] = State.HIT
                self.hitpoints -= 1
            return True
        return False

    def show(self, screen, draw_ships=True):
        screen.blit(self.background, self.position)
        if draw_ships:
            for ship in self.ships:
                ship.show(screen)

        for i in range(Grid.rows):
            for j in range(Grid.columns):
                if self.grid[i][j] == State.HIT:
                    screen.blit(self.explosion, self.convert_to_coordinates((i, j)))
                elif self.grid[i][j] == State.MISS:
                    screen.blit(self.smoke, self.convert_to_coordinates((i, j)))

    def show_selection(self, screen, indexes):
        if indexes is not None:
            screen.blit(self.red_square, self.convert_to_coordinates(indexes))
